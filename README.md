## Installation

### Linux (Ubuntu 16/18)

```
cd installation/linux
source install-ubuntu.sh
```

[Script's tree description](./linux-installer-script-tree.md)

#### Run ARMSim#

Run `/usr/local/ARMSim` or simply `ARMSim` if you have added `/usr/local` to `$PATH`

### Windows

Run `installation/windows/Installer.msi`
